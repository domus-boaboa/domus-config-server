package org.boaboa.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class DomusConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomusConfigServerApplication.class, args);
	}
}
